part of text_link;

class TextLink extends StatelessWidget {
  final String text;
  final TextStyle? textStyle;
  const TextLink({Key? key, required this.text, this.textStyle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return HtmlWidget(
      _getHtmlFromWrapingLinksAndPhoneNumbers(text),
      textStyle: textStyle,
      onTapUrl: (url) async => await launchUrl(Uri.parse(url)),
    );
  }
}

String _getHtmlFromWrapingLinksAndPhoneNumbers(String text) {
  // Regular expressions to find links and phone numbers in text
  final RegExp linkRegex =
      RegExp(r"(https?://(?:www\.)?[a-zA-Z0-9\-\.\_\?\=\+\@\&\/]+)");
  final RegExp phoneRegex = RegExp(r"(\b(0\d{9,11}))");

  // Replace links with <a> tags
  text = text.replaceAllMapped(linkRegex, (match) {
    String link = match.group(0) ?? '';
    return '<a href="$link">$link</a>';
  });

  // Replace phone numbers with <a> tags
  text = text.replaceAllMapped(phoneRegex, (match) {
    String phoneNumber = match.group(0) ?? '';
    return '<a href="tel:$phoneNumber">$phoneNumber</a>';
  });

  return text;
}
