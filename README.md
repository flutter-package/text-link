#package/text_link

#### Related package

- [url_launcher](https://pub.dev/packages/url_launcher)
- [flutter_html: ^1.0.2](https://pub.dev/packages/flutter_html/versions/1.0.2)

#### Description

- TextLink is a library designed to enhance text displayed in Flutter apps. It takes a String of text as input and returns a Widget. If the input text contains **phone numbers** or **URLs**, TextLink will automatically highlight them. Users can then click on these highlighted elements to either visit the linked website or initiate a phone call directly from the app.

#### Installation

| Version | Note                                  |
| ------- | ------------------------------------- |
| 1.0.0   | use for old project, min sdk < 2.12.0 |
| 2.0.0   | support null-safety                   |

```yaml
dependencies:
  flutter:
    sdk: flutter
  text_link:
    git:
      url: https://gitlab.com/flutter-package/text-link.git
      ref: main # or you can use specify tag
```

#### Usage

##### Import

```dart
import 'package:text_link/text_link.dart';
```

##### Example

```dart
@override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('test screen'),
      ),
      body: Center(
        child: TextLink(
          text: 'This is a https://www.youtube.com/ link and this is phone number: 0123456789',
        ),
      ),
    );
  }
```

#### Demo

<img src="https://gitlab.com/flutter-package/text-link/-/raw/main/demo_text_link.png?ref_type=heads">
